#!/usr/bin/env python3

from time import sleep
import random
import json

DEBUG = True

class RingData():
    # Ring stress value from 0 to 100
    stress_level = None
    # Label to differentate the ring. e.g. "John's Modd Ring"
    label = None
    #Ring ID in the Helvar API
    uid = None

    def __init__(self):
        # Reset variables to make sure they are populated when dumping JSON
        self.stress_level = None
        self.label = None
        self.uid = None

    def __str__(self):
        x = {
            'uid': self.uid,
            'label': self.label,
            'stressLevel': self.stress_level,
        }

        return str(x)

    def __repr__(self):
        x = {
            'uid': self.uid,
            'label': self.label,
            'stressLevel': self.stress_level,
        }
        #x = json.dumps(x)
        return str(x)

def ringHandlerWorker(shared):
    """This is the worker thread to gather the ring data from Helvar API

    Currently only spoofs data"""
    level = 0
    while(1):
        ring_list_keys = list(shared["rings"])
        for key in ring_list_keys:
            new_level = shared["rings"][key].stress_level + random.randint(-5, 5)
            if new_level > 100:
                new_level = 100
            if new_level < 0:
                new_level = 0

            if DEBUG:
                print("Updating <%s> with new value %d" \
                    %(shared["rings"][key].label, new_level))
                shared["rings"][key].stress_level = new_level

        sleep(1)

def addRing(shared, label, uid, initial_level=30):
    """Add a ring to the shared object"""
    ring = RingData()

    ring.label = label
    ring.stress_level = initial_level
    ring.uid = uid

    shared["rings"][str(uid)] = ring


def ringsToJSON(shared):
    """ This will serialize the ring dict to JSON """
    d = {}
    keys = list(shared["rings"])
    for key in keys:
        temp_dict = shared["rings"][key].__dict__
        d[key] = temp_dict

    return json.dumps(d)

if __name__ == "__main__":
    pass