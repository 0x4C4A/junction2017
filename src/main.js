import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'

import 'vuetify/dist/vuetify.css'

import ColorSliders from './components/ColorSliders.vue'
import LampEmulator from './components/LampEmulator.vue'
import LampList from './components/LampList.vue'

Vue.use(Vuetify)
Vue.use(VueResource)

Vue.component('color-sliders', ColorSliders);
Vue.component('lamp-emulator', LampEmulator);
Vue.component('lamp-list', LampList);

new Vue({
  el: '#app',
  render: h => h(App)
})
