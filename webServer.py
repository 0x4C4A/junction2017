import os
from http.server import BaseHTTPRequestHandler, HTTPServer

import helvar
import ringHandler

def webServerWorker(shared):
    class HTTPHandler(BaseHTTPRequestHandler):
        def _send_headers(self, contentType = 'text/html'):
            self.send_response(200)
            self.send_header('Content-type', contentType)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.end_headers()

        def do_GET(self):
            print(self.path)
            if '?' not in self.path:
                path = self.path
            else:
                path, args = self.path.split('?')
                tempArgs = args.split('&')
                args = {}
                for arg in tempArgs:
                    key, val = arg.split('=')
                    args[key] = val

            if path == '/get/dali':
                self._send_headers(contentType = 'application/json')
                self.wfile.write(bytes(str(shared['daliInfo']), encoding='UTF-8'))

            elif path == '/get/lamps':
                self._send_headers(contentType = 'application/json')
                first = True
                json = "["
                for lamp in shared['lamps']:
                    if not first:
                        json += ','
                    else:
                        first = False
                    json += str(lamp).replace("'",'"')
                json += "]"
                self.wfile.write(bytes(json, encoding='UTF-8'))

            elif path == '/get/points':
                self._send_headers(contentType = 'application/json')
                self.wfile.write(bytes(str(shared['flir_data'].points).replace("'",'"').replace('(','[').replace(')',']'), encoding='UTF-8'))

            elif path == '/get/rings':
                self._send_headers(contentType = 'application/json')
                #self.wfile.write(bytes(str(shared["rings"]), encoding = 'UTF-8'))
                self.wfile.write(bytes(ringHandler.ringsToJSON(shared), encoding = 'UTF-8'))

            elif path == '/send':
                print(args)
                x, y = helvar.convertRGBToXY(args['red'], args['green'], args['blue'])
                helvar.sendCommand(x = x, y = y, level = args['level'], device = args['device'].split(';'))
                self._send_headers(contentType = 'application/json')
                self.wfile.write(bytes(str(args), encoding='UTF-8'))

            elif path == '/pic':
                self._send_headers(contentType = 'image/png')
                self.wfile.write(shared['flir_data'].pic_raw)

            elif path == '/pic2':
                self._send_headers(contentType = 'image/png')
                self.wfile.write(shared['flir_data'].pic_blobs)

            elif os.path.isfile('.'+path):
                file = open('.'+path, 'rb')
                self._send_headers()
                self.wfile.write(file.read())

            else:
                self.send_response(404)


    """thread worker function"""
    httpd = HTTPServer(("", 8090), HTTPHandler)
    print("Webserver started!")
    while(1):
        httpd.handle_request()
