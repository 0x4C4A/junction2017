#!/usr/bin/env python3
"""Try to determine people coordinates from FLIR image"""
import cv2
import numpy as np

# Tresholds for the tetected people in grayscale image
# TODO: Threshold should be calculated above average image value, not a static value.
# If can't then there are no people - we take into acount that FLIR does
# auto color adjustment
GRAY_THRESHOLD = 120

IMAGE_FILE = "test.png"

img = cv2.imread(IMAGE_FILE, 0)

_, threshImage = cv2.threshold(img, GRAY_THRESHOLD, 255, cv2.THRESH_BINARY)
threshImage = cv2.erode(threshImage, None, iterations=2)
threshImage = cv2.dilate(threshImage, None, iterations=2)

mask = np.zeros(threshImage.shape, dtype="uint8")

# # find contours in the mask and initialize the current
# # (x, y) center of the ball
cnts = cv2.findContours(threshImage.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[1]

center = None
# only proceed if at least one contour was found
for (i, c) in enumerate(cnts):
	((cX, cY), radius) = cv2.minEnclosingCircle(c)
	print((cX, cY, radius))
#	cv2.circle(img, (int(cX), int(cY)), int(radius),
#		(0, 0, 0), 1)
	cv2.circle(threshImage, (int(cX), int(cY)), int(radius),
		(0, 0, 0), 1)


cv2.imshow("threshName", threshImage)
cv2.waitKey(0)
