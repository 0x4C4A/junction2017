#!/usr/bin/env python

import sys
import numpy as np
import cv2
from pylepton import Lepton
import requests
from optparse import OptionParser
import boto3


s3 = boto3.resource('s3')
FILE = "/home/pi/pylepton_scripts/data_dump/thermaldata_stream.png"


# Captures an image with FLIR Lepton IR camera. It will be returned as a matrix of integers.
def capture(flip_v = False, device = "/dev/spidev0.0"):
       with Lepton(device) as l:
                a,_ = l.capture()
		cv2.flip(a,1,a)         # Flips image horizontaly
	        cv2.normalize(a, a, 0, 65535, cv2.NORM_MINMAX)
        	np.right_shift(a, 8, a)
	        return np.uint8(a)


if __name__ == '__main__':
	# Load options	
	usage = "usage: %prog [options]"
	parser = OptionParser(usage=usage)
        parser.add_option("-d", "--device",
                dest="device", default="8BE3",
                help="ID number of the device which you want to control.")
	parser.add_option("-s", "--server",
		dest="server", default="https://9h21iuqi9g.execute-api.eu-central-1.amazonaws.com/v1/gateways/",
		help="specify the server with the API")
	parser.add_option("--spi",
		dest="spi", default="/dev/spidev0.0",
		help="specify the spi device node (might be /dev/spidev0.1 on a newer device)")
        parser.add_option("-g", "--gateway",
                dest="gateway", default="34",
                help="UUID of the gateway which is used to transfer the message to the Bluetooth mesh.")

	(options, args) = parser.parse_args()

	# Create new window
	cv2.namedWindow('IR camera', cv2.WND_PROP_FULLSCREEN)
	cv2.setWindowProperty('IR camera', cv2.WND_PROP_FULLSCREEN, cv2.cv.CV_WINDOW_FULLSCREEN)
	
	# Loop until user presses s
	while(1):

		image = capture(device = options.spi)
		image_heat = cv2.applyColorMap(255-image, cv2.COLORMAP_HOT)
		cv2.imshow('IR camera',image_heat)

		# save the current image
		cv2.imwrite(FILE, image)
		
		# Upload image to S3
		with open(FILE, 'rb') as data:
			s3.Bucket('helvar-stream').put_object(Key='thermaldata.png', Body=data, ACL='public-read', StorageClass='REDUCED_REDUNDANCY')
	
		# print(cv2.waitKey(1))
		if cv2.waitKey(33) == ord('s'):
			print("Key press detected. Exiting.")
			exit(0)
