#!/usr/bin/env python3
"""This is a testing file for opencv"""
import cv2
import urllib.request

# At this address at all times the most recent captured FLIR frame is stored
url = "https://s3-eu-west-1.amazonaws.com/helvar-stream/thermaldata.png"

flir_file = "flir_file.png"

with urllib.request.urlopen(url) as response, open(flir_file, 'wb') as out_file:
	# 'data' is a PNG file
    data = response.read() # a `bytes` object
    out_file.write(data)

img = cv2.imread(flir_file)

if img is not None:
	cv2.imshow('image',img)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
else:
	print("Image not recognized")
	