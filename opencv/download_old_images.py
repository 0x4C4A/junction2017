#!/usr/bin/env python3
"""Download all the versions of images stored on the webserver"""
import urllib.request
import requests
import re
import xml.etree.ElementTree as ET


versions_url = "https://s3-eu-west-1.amazonaws.com/helvar-stream/?versions&prefix=thermaldata.png"

pumpiic = "https://s3-eu-west-1.amazonaws.com/helvar-stream/thermaldata.png?versionId="

flir_base = "FLIR/img_"

r = requests.get(versions_url)

# Need to get rid of the tag in the first line of xml
text = r.text
text = re.sub(r" xmlns=\".*\"", "", text)

with open("xml.xml", "w") as f:
   f.write(text)

with open("xml.xml", "r") as f:
    tree = ET.parse("xml.xml")

elem = tree.getroot()

counter = 0

print()
print ("Downloading most recent 1000 images..")
for ver in elem.iter("VersionId"):
    url = pumpiic + ver.text
    name = flir_base + str(counter) + ".png"
    print ("Downloading image %d" %counter)
    with urllib.request.urlopen(url) as response, open(name, 'wb') as out_file:
        data = response.read()
        out_file.write(data)
    counter += 1
    