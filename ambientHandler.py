#!/usr/bin/env python3

from time import sleep
from math import sin, radians

class AmbientData():
    #Ambient light intensity value from 100 to 254
    intensity = None
    #
    label = None

def ambientHandlerWorker(shared):
    intensity = 100
    cnt = 0
    
    while(1):
        cnt += 1
        intensity = round(100 + 70*( 1 + sin(radians(cnt)) )) 
        shared['AmbientData'].intensity = intensity
        sleep(2)
        if cnt >= 360:
            cnt = 0
        

if __name__ == "__main__":
    # Test out some things
    the_data = AmbientData()
    shared = {}
    shared['AmbientData'] = the_data

    ambientHandlerWorker(shared)