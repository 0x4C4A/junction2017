""" """
import math
import time

import helvar

def pushHelvarData(lamp):
    """Push data from 'lamp' obhject to Helvar API"""
    x, y = helvar.convertRGBToXY(lamp.color_rgb[0],
                                 lamp.color_rgb[1],
                                 lamp.color_rgb[2])
    print ("Calculated XY = <%d  %d>" %(x,y))
    helvar.sendCommand(x, y, lamp.device, lamp.brightness)

def calcBrightness(shared):
    """ Calculates the appropriate lamp brightness value based on the
    ambient light level"""

    # Ambient brightness values can be from 0 (total darkness) to 100
    # (really sunny)
    # Since the ambient sensor is faked, there is no need for a conversion function
    return shared["AmbientData"].intensity

def logicWorker(shared):
    """The main logic thread for the program"""

    while(1):
        # 1. Only lamps where there are people nearby should be turned on
        turn_ons = lampInRange(shared)
        print("turn_ons = ", turn_ons)

        # Set the brightness values.
        # There should be some calculation, but currently we just set a static value
        for index, lamp in enumerate(shared["lamps"]):
            if index in turn_ons:
                shared["lamps"][index].brightness = calcBrightness(shared)
            else:
                shared["lamps"][index].brightness = 0

        for lamp in shared["lamps"]:
            print("Lamp <%s> color = <%d, %d, %d>, brigtness = %d" \
                %(lamp.label, lamp.color_rgb[0],
                  lamp.color_rgb[1],lamp.color_rgb[2],
                  lamp.brightness))

        # Calculate the color temperature based on the assigned Stress Rings
        for index, lamp in enumerate(shared["lamps"]):
            stress_list = []
            for uid in lamp.rings:
                stress_list.append(shared["rings"][str(uid)].stress_level)

            print (stress_list)
            temperature = calcTemperature(max(stress_list))
            rgb = colorTempToRGB(shared, temperature)
            r = rgb["r"]
            g = rgb["g"]
            b = rgb["b"]
            shared["lamps"][index].set_color(r, g, b)
            print("Stress list = ", stress_list)
            print("Temp = %d, R=%d,G=%d,B=%d" %(temperature, r, g, b))

        print()
        # 2. Lamp brightness should be based on the ambient brightness that
        #    received from some light intensity sensor

        # 3. Lamps should 

        ## Send the new lamp parameters to the smart lamps
        for index, lamp in enumerate(shared["lamps"]):
          if lamp.device is not None:
              pushHelvarData(lamp)

        time.sleep(0.5)

def calcTemperature(stress_level):
    MIN = 1000
    MAX = 6000

    temp = ((MAX - MIN)/100*stress_level + MIN)

    return temp

def lampInRange(shared):
    lampOn = [] #list of lamps that need to switch on


    # Loop trough all lamps
    for i in range(len(shared['lamps'])):

        lampX = shared['lamps'][i].loc_x
        lampY = shared['lamps'][i].loc_y
        lampR = shared['lamps'][i].display_radius

        # Loop trough all flir points
        for point in shared['flir_data'].points:
            objX = point[0]
            objY = point[1]
            objR = point[2]

            #Check if flir point center inside lamp radiuss #(x-cx)^2 -(y-cy)^2<r^2
            if(math.pow((objX-lampX),2) - math.pow((objY-lampY),2) < math.pow(lampR,2)):
                lampOn.append(i)
                break
            else:
                #Check if circles overlapse
                if(math.pow((lampR-objR),2) <= (math.pow((lampX-objX),2)+ math.pow((lampY-objY),2))):
                    if((math.pow((lampX-objX),2)+ math.pow((lampY-objY),2)) <= math.pow((lampR+objR),2)):
                        lampOn.append(i)
                        break

    return lampOn

def colorTempToRGB(shared, kelvin):
    temp = kelvin / 100;
    red, green, blue = None, None, None

    if(temp <= 66):
        red = 255
        green = 99.4708025861 * math.log(temp) - 161.1195681661

        if(temp <= 19):
            blue = 0
        else:
            blue = temp - 10
            blue = 138.5177312231 * math.log(blue) - 305.0447927307
    else:
        red = temp - 60
        red = 329.698727446 * math.pow(red, -0.1332047592)
        green = temp - 60
        green = 288.1221695283 * math.pow(green, -0.0755148492 )
        blue = 255

    return {
        "r": clamp(red, 0, 255),
        "g": clamp(green, 0, 255),
        "b": clamp(blue, 0, 255)
    }


def clamp(x, min, max):

    if(x < min): return min
    if(x > max): return max

    return x

def luminenceToLevel(shared, luminence):
    height = 3
    diameter = 2.73
    maxLumens = 6000
    surface = math.pi * ((diameter/2)**2)
    lumens = luminence * surface
    dimLevel = lumens/maxLumens
    level = (253/3) * (math.log10(dimLevel) + 1) + 1
    return level




