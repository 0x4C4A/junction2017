#!/usr/bin/env python3
"""OpenCV worker for filtering the FLIR images"""
import cv2
import numpy as np
import time
import urllib.request

DEBUG = 0

class FlirData():
    # 'points' is a list of tuples with (x, y, radius, timestamp) data
    # The tuple is updated only if nrew valid points (blobs) are found in the image
    points = []
    # pic_raw is the RAW captured FLIR image encoded in PNG
    pic_raw = None
    # pic_blobs is the thresholded white blobs encoded as PNG
    pic_blobs = None


def flirHandlerWorker(shared):
    """

    The 'shared' argument shall contain a dict keyword 'flir_data' that
    is a FlirData() class with data values"""

    # Here the most recent FLIR image is stored
    url = "https://s3-eu-west-1.amazonaws.com/helvar-stream/thermaldata.png"
    flir_file = "flir_file.png"

    # Tresholds for the tetected people in grayscale image
    # TODO: Threshold should be calculated above average image value, not a static value.
    # If can't then there are no people - we take into acount that FLIR does
    # auto color adjustment
    GRAY_THRESHOLD = 120
    
    READ_FROM_FOLDER = True
    FIRST_NORMAL_PICTURE_INDEX = 8
    LAST_NORMAL_PICTURE_INDEX = 278
    
    imgCounter = FIRST_NORMAL_PICTURE_INDEX 

    while(1):

        if not READ_FROM_FOLDER:
            try:
                with urllib.request.urlopen(url) as response, open(flir_file, 'wb') as out_file:
                    # 'data' is a PNG file
                    data = response.read() # a `bytes` object
                    out_file.write(data)
            except:
                print("Exception while getting image!")
                raise
                #continue

        if DEBUG:
            print("Got a new image")

            
            
        if READ_FROM_FOLDER:
            imgIndex = str(imgCounter)
            local_file =  "bildes/thermaldata_stream" + imgIndex + ".png" 
            img = cv2.imread(local_file, 0)
            imgCounter +=1
            if imgCounter > LAST_NORMAL_PICTURE_INDEX:
                imgCounter = FIRST_NORMAL_PICTURE_INDEX
            time.sleep(0.5)
        else:
            img = cv2.imread(flir_file, 0)
            

        _, threshImage = cv2.threshold(img, GRAY_THRESHOLD, 255, cv2.THRESH_BINARY)
        threshImage = cv2.erode(threshImage, None, iterations=2)
        threshImage = cv2.dilate(threshImage, None, iterations=2)

        cnts = cv2.findContours(threshImage.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[1]

        # only proceed if at least one contour was found
        temp_data = []
        for (i, c) in enumerate(cnts):
            if DEBUG:
                print("Found a circle")
            ((cX, cY), radius) = cv2.minEnclosingCircle(c)

            timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
            temp_data.append((cX, cY, radius, timestamp))

            if DEBUG:
                # Draw the circles
                cv2.circle(img, (int(cX), int(cY)), int(radius),
                    (0, 0, 0), 1)


        if len(temp_data) != 0:
            if DEBUG:
                print("Appending following data to flir data:")
                print(temp_data)
            # Replace data in the shared dict
            shared['flir_data'].points = temp_data

            shared['flir_data'].pic_raw = cv2.imencode(".png", img)[1].tostring()

            # Do some processing on blobs image before retuning it to shared dict
            pic_blobs = cv2.cvtColor(threshImage, cv2.COLOR_GRAY2RGBA)
            pic_blobs[np.where((pic_blobs==[0,0,0,255]).all(axis=2))] = [0, 0, 0, 0]
            pic_blobs[np.where((pic_blobs==[255,255,255,255]).all(axis=2))] = [0, 255, 0, 255]
            
            shared['flir_data'].pic_blobs = cv2.imencode(".png", pic_blobs)[1].tostring()
            #print(shared['flir_data'].pic_raw)

        if DEBUG:
            # Show the image
            #cv2.imshow('image',img)
            cv2.imshow('image',pic_blobs)
            key = cv2.waitKey(1) & 0xFF

            # if the 'q' key is pressed, stop the loop
            if key == ord("q"):
                break


if __name__ == "__main__":
    # Test out some things
    the_data = FlirData()
    shared = {}
    shared["flir_data"] = the_data

    flirHandlerWorker(shared)