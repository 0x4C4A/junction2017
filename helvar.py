import requests

urlStart = 'https://5nk8a0rfn5.execute-api.eu-west-1.amazonaws.com/v1'
def convertRGBToXY(red, green, blue):
    red   = float(red)
    green = float(green)
    blue  = float(blue)

    # if red > 0.04045:
    #     red = pow((red+ 0.055) / (1.0 + 0.055), 2.4)
    # else:
    #     red = red/ 12.92

    # if green > 0.04045:
    #     green = pow((green + 0.055) / (1.0 + 0.055), 2.4)
    # else:
    #     green = green / 12.92

    # if blue > 0.0405:
    #     blue = pow((blue + 0.055) / (1.0 + 0.055), 2.4)
    # else:
    #     blue = blue / 12.92

    #RGB values to XYZ using the Wide RGB D65 conversion formula
    X = red * 0.664511 + green * 0.154324 + blue * 0.162028;
    Y = red * 0.283881 + green * 0.668433 + blue * 0.047685;
    Z = red * 0.000088 + green * 0.072310 + blue * 0.986039;

    #Calculate the xy values from the XYZ values
    x = (X / (X + Y + Z));
    y = (Y / (X + Y + Z));

    x = round(32767.0 * x);
    y = round(32767.0 * y);
    return x, y


def sendCommand(x = None, y = None, device = None, level = None):
    url = urlStart +'/command?'
    if level is not None:
        level = int(float(level))
        url += 'level=' + str(level) + '&'
    if x is not None:
        url += 'colour_x=' + str(x) + '&'
    if y is not None:
        url += 'colour_y=' + str(y) + '&'
    if device is not None:
        url += 'device=' + str(device) + '&'

    print(url)
    f = requests.get(url)

    daliInfo = getDaliInfo(device = device, limit = 1)

def getDaliInfo(device = None, limit = None):
    url = urlStart + '/dali-data?'
    if device:
        url += 'device=' + str(device) + '&'
    if limit:
        url += 'limit=' + str(limit) + '&'

    return requests.get(url).json()[0]
