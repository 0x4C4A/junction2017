""" Everything related to lamps """
import json
from numpy import multiply

class lampData():
	loc_x = None
	loc_y = None

	# Max value is 255
	brigtness = 10

	# RGB color. Should be 100% saturated all the time
	color_rgb = (0, 0, 0)
	display_radius = None

	# Label to differentiate between lamps. Human readable
	label = None

	# Helvar API lamp device
	device = None
	
	# List of assigned rings to lamps
	rings = []

	def set_color(self, r, g, b):
		# Convert RGB values to legal rgb values for the Helvar lamp
		colors = [float(r), float(g), float(b)]
		maxval = max(colors)
		# Use integer division
		coeff = 255./maxval
		multiply(colors, coeff)
		self.color_rgb = (int(colors[0]),
						  int(colors[1]),
						  int(colors[2]))

	def __str__(self):
		x = {
			'x': self.loc_x,
			'y': self.loc_y,
			'r': self.display_radius,
			'label': self.label,
			'col': '#%02X%02X%02X' % self.color_rgb,
			'l': self.brightness,
			'rings' : self.rings

		}
		return str(x)

def getLampJSON(shared):
	# Return all lamp data as a JSON string
	# Construct a list of lam objects as dict's
	lst = []
	for lamp in shared["lamps"]:
		lst.append(json.dumps(lamp.__dict__))

	return json.dumps(lst)

def addLamp(shared, x, y, radius=5, label=None, device=None, flags=None):
	# Add a new lamp to the shared lamp list
	lamp = lampData()

	# TODO: Validate coordinate data
	if (x >= shared["COORDS_LIMITS"]["x_min"]) and \
	    (x <= shared["COORDS_LIMITS"]["x_max"]):
		lamp.loc_x = x
	if (y >= shared["COORDS_LIMITS"]["y_min"]) and \
	    (y <= shared["COORDS_LIMITS"]["y_max"]):
		lamp.loc_y = y

	lamp.display_radius = radius
	lamp.label = label
	lamp.flags = flags
	lamp.device = device

	shared["lamps"].append(lamp)


if __name__ == "__main__":
	# Test out JSON-ability
	a = lampData()
	shared = {"lamps" : [a, a, a]}
	print (json.dumps(a.__dict__))
	print (getLampJSON(shared))
