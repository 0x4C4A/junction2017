#!/usr/bin/env python3
import threading
from webServer import webServerWorker

import helvar
import flirHandler
import logic
import lamps
import workspaces
import ringHandler
import ambientHandler

# Limits to the coordinate system used for calculations
# Basically the FLIR image area
# (x_min, y_min, x_max, y_max)
COORDS_LIMITS = {
	"x_min" : 0,
	"y_min" : 0,
	"x_max" : 80,
	"y_max" : 60
}

flir_data_obj = flirHandler.FlirData()
ambient_data_obj = ambientHandler.AmbientData()

shared = {}
shared['daliInfo'] = helvar.getDaliInfo()
shared["flir_data"] = flir_data_obj
shared["COORDS_LIMITS"] = COORDS_LIMITS
shared["lamps"] = []
shared["workspaces"] = []
shared["rings"] = {}
shared["AmbientData"] = ambient_data_obj

# Add some sample lamps
lamps.addLamp(shared, 20, 15, label="Kim", device=0)
lamps.addLamp(shared, 60, 15, label="Vladimir", device=3)
lamps.addLamp(shared, 20, 45, label="Adolf", device=8)
lamps.addLamp(shared, 60, 45, label="Donald")

# Add some sample values to lamps
shared["lamps"][0].set_color(255, 255, 255)
shared["lamps"][1].set_color(255, 255, 255)
shared["lamps"][2].set_color(255, 255, 255)
shared["lamps"][3].set_color(255, 255, 255)

shared["lamps"][0].brightness = 254
shared["lamps"][1].brightness = 254
shared["lamps"][2].brightness = 254
shared["lamps"][3].brightness = 254

shared["lamps"][0].display_radius = 30
shared["lamps"][1].display_radius = 30
shared["lamps"][2].display_radius = 30
shared["lamps"][3].display_radius = 30

# Add a couple of StressRings
ringHandler.addRing(shared, label="John's Ring", initial_level=20, uid=101)
ringHandler.addRing(shared, label="Samantha's Ring", initial_level=40, uid=102)
ringHandler.addRing(shared, label="Carl's Ring", initial_level=60, uid=103)
ringHandler.addRing(shared, label="Elon's Ring", initial_level=10, uid=104)
ringHandler.addRing(shared, label="Albert's Ring", initial_level=50, uid=105)
ringHandler.addRing(shared, label="Layla's Ring", initial_level=70, uid=106)
ringHandler.addRing(shared, label="Eddard's Ring", initial_level=0, uid=107)

# shared["lamps"][0].rings = [101, 102]
# shared["lamps"][1].rings = [102, 103, 104]
# shared["lamps"][2].rings = [104, 105]
# shared["lamps"][3].rings = [105, 106, 107]

shared["lamps"][0].rings = [105]
shared["lamps"][1].rings = [107]
shared["lamps"][2].rings = [106]
shared["lamps"][3].rings = [105, 106, 107]

threads = []
webServerThread = threading.Thread(target=webServerWorker, args=(shared,))
flirhandlerThread = threading.Thread(target=flirHandler.flirHandlerWorker, args=(shared,))
logicThread = threading.Thread(target=logic.logicWorker, args=(shared,))
ringThread = threading.Thread(target=ringHandler.ringHandlerWorker, args=(shared,))
ambientThread = threading.Thread(target=ambientHandler.ambientHandlerWorker, args=(shared,))


threads.append(webServerThread)
threads.append(flirhandlerThread)
threads.append(logicThread)
threads.append(ringThread)
threads.append(ambientThread)

webServerThread.start()
flirhandlerThread.start()
logicThread.start()
ringThread.start()
ambientThread.start()

thingy = 0
while(1):
    thingy += 0.3;
    if thingy > 1.0:
        thingy = 0;
    #var = helvar.convertRGBToXY(thingy, sys.argv[2], sys.argv[3])
    #helvar.sendCommand(var['x'], var['y'], level=sys.argv[4],   device = 3)
    #helvar.sendCommand(var['x'], var['y'], level=int(sys.argv[4])-1, device = 3)
    shared['daliInfo'] = helvar.getDaliInfo()


for thread in threads:
    thread.join()
