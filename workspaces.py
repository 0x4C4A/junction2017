""" Everything related to workspaces """
class workspaceData():
	# List of tuples (x, y) that define an arbitrary form in xy coordinates
	coords = []

	# Color tuple (r, g, b) that define the AntiStress accent color
	favorite_color = ()

	# Weight how much to mix the color in 0..100, where 0 = don't add
	# the color and 100 means turn the light completely to the favorite
	# color in high stress situations
	favorite_color_weight = 50

	# Above what stress level start mixing in the favorinte Antistress color
	stress_threshold = 50

	# String label to identify the workspace
	# Label must NOT be empty
	label = None

	# Labels of lamps that are assigned to this workspace.
	# Can be multiple
	lamps = []

	# Label of the StressRing that is assigned to this workspace.
	ring = None

	# Lamp assignment to workspaces.
	# Each 
	def addLamp(self, shared, lamp_label):
		"""Add lamp with the label 'lamp_label' to this workspace

		This will remove the lamp from all the other workspaces"""

		# Check if this workspace has a label
		if self.label is None:
			raise WorkspaceError("This workspace has no label assigned")

		# Remove this lamp from all the workspaces
		# This should prevent adding a lamp to multiple workspaces
		for i, wsp in enumerate(shared["workspaces"]):
			if lamp_label in wsp.lamps:
				shared["workspaces"][i].lamps.remove(lamp_label)

		# Check if such lamp exists
		lamp_found = 0
		for i, lamp in enumerate(shared["lamps"]):
			if lamp.label == lamp_label:
				lamp_found = 1
				if lamp_label not in self.lamps:
					self.lamps.append(lamp_label)

		if lamp_found == 0:
			raise WorkspaceError("Lamp with label <%s> does not exist" %lamp_label)

	def removeLamp(self, lamp_label):
		"""Remove lamp from this workspace"""
		if lamp_label in self.lamps:
			self.lamps.remove(lamp_label)


	# def addRing(self, shared, ring_label):
	# 	"""Add a Stress ring to this workspace"""
	# 	# Check if such ring exists
	# 	self.ring = None
	# 	for i, ring in enumerate(shared["rings"]):
	# 		if ring.label == ring_label:
	# 			self.ring = ring_label

	# 	if self.ring is None:
	# 		raise WorkspaceError("Ring with label <%s> does not exist" %ring_label)

	def removeRing(self):
		"""Remove Stress ring from this workspace"""
		self.ring = None

def getWorkspaceJSON(shared):
	# Return all workspace data as a JSON string
	# Construct a list of workspace objects as dict's
	lst = []
	for wsp in shared["workspaces"]:
		lst.append(json.dumps(wsp.__dict__))

	return json.dumps(lst)

class WorkspaceError(Exception):
	"""Exception class for lamp errors"""
	pass