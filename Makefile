.PHONY: all

all: python stopNode
	npm run dev &

python: stopPython
	python3 main.py &

stopPython:
	killall -9 Python python3 || echo "Python's down already"

stopNode:
	killall -9 node || echo "Node's down already"

stop: stopNode stopPython
